
const express = require('express');
const mongoose = require('mongoose');
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");
const bcrypt = require('bcrypt');

const cors = require('cors');
const app = express();

require('dotenv').config();

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

const DB_CONNECTION = process.env.MONGO_URI;
mongoose.connect(DB_CONNECTION, 
    { useNewUrlParser: true, useUnifiedTopology: true }
    )
    .then(() => console.log('Connected to MongoDB'))
    .catch(err => console.error('Could not connect to MongoDB', err));

app.get("/b6", (req, res) => {
    res.send("Hello world")
})

app.use("/b6/users", userRoutes);
app.use("/b6/products", productRoutes);

const PORT = process.env.PORT || 4006;
app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
});