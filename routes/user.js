const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.js')

const auth = require("../auth");
const {verify, verifyAdmin} = auth;



router.post("/", userController.registerUser);

router.post("/login", userController.loginUser);

router.get("/details", verify, userController.getProfile);

router.put("/:userId/set-as-admin", verify, verifyAdmin, userController.updateAdmin);

router.put("/update-password", verify, userController.updatePassword);

// s52 routes start
router.get("/get-cart", verify, userController.getUserCart);

router.post("/add-to-cart", verify, userController.addToCart);

router.post("/update-cart-quantity", verify, userController.updateCartQuantity);
// s52 routes end

// s53 start
router.post("/:productId/remove-from-cart", verify, userController.removeCartProduct);

router.post("/clear-cart", verify, userController.clearCart);
// s53 end


// s54 start
router.post("/checkout", verify, userController.createOrder);


router.get("/my-orders", verify, userController.retrieveUserOrders);


router.get("/all-orders", verify, verifyAdmin, userController.retrieveAllUserOrders);
// s54 end

module.exports = router;
