const express = require('express');
const router = express.Router();
const productController = require('../controllers/product');
const auth = require("../auth");
const {verify, verifyAdmin } = auth;

// s51  Create Product (Admin only)
router.post("/", verify, verifyAdmin, productController.createProduct);

// s51  Retrieve all products (Admin only)
router.get("/all", verify, verifyAdmin, productController.getAllProducts);

// s 53 start
router.get("/searchByName", productController.searchProductByName);

router.get("/searchByPrice", productController.searchProductByPrice);
// s53 end

// s51  Retrieve all active products (All users)
router.get("/", verify, productController.getActiveProducts);

// s51  Retrieve a single product (All users)
router.get("/:productId", verify, productController.getSingleProduct);

// s51  Update product info (Admin only)
router.put("/:productId/update", verify, verifyAdmin, productController.updateProduct);

// s51  Archive product (Admin only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

// s51  Activate product (Admin only)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);

// // //s 53 start
// router.get("products/searchByName", productController.searchProductByName);

// router.get("products/searchByPrice", productController.searchProductByPrice);
// // //s53 end

module.exports = router;


