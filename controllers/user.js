const User = require('../models/user');

//s52 start 
const Cart = require('../models/cart'); 
//s52 end 

//s54 start
const Order = require('../models/order');
//s54 end 

const bcrypt = require("bcrypt");
const auth = require('../auth');


module.exports.registerUser = (req, res) => {
    
    User.findOne({ email: req.body.email })
        .then(existingUser => {
            if (existingUser) {
                return res.status(400).send({ message: 'User already exists' });
            }

            
            const user = new User({
                firstName: req.body.firstName,
                lastName: req.body.lastName,
                email: req.body.email,
                password: bcrypt.hashSync(req.body.password, 10),
                mobileNo: req.body.mobileNo
            });

            user.save()
                .then(() => {
                    res.status(201).send({ message: 'User registered successfully' });
                })
                .catch(saveErr => {
                    console.error(saveErr); // Log the error for debugging
                    res.status(500).send({ error: 'Server error' });
                });
        })
        .catch(err => {
            console.error(err); // Log the error for debugging
            res.status(500).send({ error: 'Server error' });
        });
};

module.exports.loginUser = (req, res) => {
    User.findOne({ email: req.body.email })
        .then(user => {
            if (!user) {
                return res.status(404).send({ error: "No Email Found" });
            }

            bcrypt.compare(req.body.password, user.password)
                .then(isPasswordCorrect => {
                    if (isPasswordCorrect) {
                        return res.status(200).send({ access: auth.createAccessToken(user) });
                    } else {
                        return res.status(401).send({ message: "Email and/or password do not match" });
                    }
                })
                .catch(bcryptErr => {
                    console.error(bcryptErr);
                    return res.status(500).send({ error: "Server error" });
                });
        })
        .catch(err => {
            console.error(err);
            return res.status(500).send({ error: "Server error" });
        });
}; 

module.exports.getProfile = async (req, res) => {
    try {
        console.log("req.user display");
        console.log(req.user);

        const user = await User.findById(req.user.id);

        if (!user) {
            return res.status(404).send({ error: 'User not found' });
        } else {
            // It's a good practice not to modify the original user object
            const userResponse = { ...user.toObject(), password: "*****" };
            return res.status(200).send({ user: userResponse });
        }
    } catch (err) {
        console.error(err); // Log the error for debugging
        return res.status(500).send({ error: 'Server error' });
    }
};

module.exports.updateAdmin = async (req, res) => {
  try {
    const { userId } = req.params; 

    
    const updatedUser = await User.findByIdAndUpdate(
      userId,
      { isAdmin: true },
      { new: true }
    );

    if (!updatedUser) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({ message: 'User updated as admin successfully', user: updatedUser });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

module.exports.updatePassword = async (req, res) => {
    try {
        // Optional: console logs for debugging
        // console.log(req.body);
        // console.log(req.user);

        const { newPassword } = req.body;

        // Extracting user ID from the authorization header
        const { id } = req.user;

        // Hashing the new password
        const hashedPassword = bcrypt.hashSync(newPassword, 10);
    
        // Updating the user's password in the database
        await User.findByIdAndUpdate(id, { password: hashedPassword });

        // Sending a success response
        res.status(200).json({ message: 'Password updated successfully' });

    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error'});
    }
};

//-----------------------------------------------------------------------s52 start

// s52 get user cart

module.exports.getUserCart = async (req, res) => {
  try {
    const userId = req.user.id;

    const userCart = await Cart.findOne({ userId });

    if (!userCart) {
      return res.status(404).json({ message: 'User cart not found' });
    }

    res.status(200).json({ userCart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

// s52 add to cart
module.exports.addToCart = async (req, res) => {
  try {
    const userId = req.user.id;
    const { productId, quantity, subtotal } = req.body;

    let userCart = await Cart.findOne({ userId });

    if (!userCart) {
      userCart = new Cart({
        userId,
        cartItems: [],
        totalPrice: 0,
      });
    }

    const existingProductIndex = userCart.cartItems.findIndex(item => item.productId === productId);

    if (existingProductIndex !== -1) {
      userCart.cartItems[existingProductIndex].quantity += quantity;
      userCart.cartItems[existingProductIndex].subtotal += subtotal;
    } else {
      userCart.cartItems.push({ productId, quantity, subtotal });
    }

    userCart.totalPrice = userCart.cartItems.reduce((total, item) => total + item.subtotal, 0);

    await userCart.save();

    res.status(200).json({ message: 'Product added to cart successfully', userCart });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

// s52 update quantity
module.exports.updateCartQuantity = async (req, res) => {
  try {
    const userId = req.user.id;
    const { productId, quantity, subtotal } = req.body;

    const userCart = await Cart.findOne({ userId });

    if (!userCart) {
      return res.status(404).json({ message: 'User cart not found' });
    }

    const existingProductIndex = userCart.cartItems.findIndex(item => item.productId === productId);

    if (existingProductIndex !== -1) {
      userCart.cartItems[existingProductIndex].quantity = quantity;
      userCart.cartItems[existingProductIndex].subtotal = subtotal;

      userCart.totalPrice = userCart.cartItems.reduce((total, item) => total + item.subtotal, 0);

      await userCart.save();

      res.status(200).json({ message: 'Cart quantity updated successfully', userCart });
    } else {
      return res.status(404).json({ message: 'Product not found in the user cart' });
    }
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

//-----------------------------------------------------------------------s52 end


//-----------------------------------------------------------------------s53 start

// Controllers
module.exports.removeCartProduct = (req, res) => {
    const userId = req.user.id;
    const productId = req.params.productId;

    // Find the user's cart
    Cart.findOne({ userId })
        .then(userCart => {
            if (!userCart) {
                return res.status(404).json({ message: "Cart not found" });
            }

            // Find the cart item to remove by product ID
            const cartItemToRemove = userCart.cartItems.find(item => item.productId === productId);

            if (!cartItemToRemove) {
                return res.status(404).json({ message: "Product not found in cart" });
            }

            // Remove the cart item from the user's cart
            const itemIndex = userCart.cartItems.indexOf(cartItemToRemove);
            userCart.cartItems.splice(itemIndex, 1);

            // Update the total price
            userCart.totalPrice -= cartItemToRemove.subtotal;

            // Save the updated cart
            return userCart.save();
        })
        .then(updatedCart => {
            return res.status(200).json({ message: "Product removed from cart", updatedCart });
        })
        .catch(err => {
            console.error(err);
            return res.status(500).json({ message: "Internal server error", error: err });
        });
};


module.exports.clearCart = (req, res) => {
    const userId = req.user.id; 

    // Find the user's cart
    Cart.findOne({ userId })
        .then(userCart => {
            if (!userCart) {
                return res.status(404).json({ message: "Cart not found" });
            }

            if (userCart.cartItems.length > 0) {
                // If cart has items, remove all items
                userCart.cartItems = [];
                userCart.totalPrice = 0;
            }

            // Save the updated cart
            return userCart.save();
        })
        .then(updatedCart => {
            return res.status(200).json({ message: "Cart cleared successfully", updatedCart });
        })
        .catch(err => {
            console.error(err);
            return res.status(500).json({ message: "Internal server error", error: err });
        });
};

//-----------------------------------------------------------------------s53 end

//-----------------------------------------------------------------------s54 start

module.exports.createOrder = async (req, res) => {
    try {
        const userId = req.user.id;

        
        const userCart = await Cart.findOne({ userId });

        if (!userCart || userCart.cartItems.length === 0) {
            return res.status(404).json({ message: "No items in the cart" });
        }

        
        const newOrder = new Order({
            userId,
            productsOrdered: userCart.cartItems,
            totalPrice: userCart.totalPrice
        });

        
        const savedOrder = await newOrder.save();

        
        userCart.cartItems = [];
        userCart.totalPrice = 0;
        await userCart.save();

        res.status(201).json({ message: 'Order created successfully', order: savedOrder });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error', error: error.message });
    }
};

module.exports.retrieveUserOrders = async (req, res) => {
    try {
        const userId = req.user.id;

        
        const userOrders = await Order.find({ userId });

        res.status(200).json({ userOrders });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error', error: error.message });
    }
};

module.exports.retrieveAllUserOrders = async (req, res) => {
    try {
        
        const allOrders = await Order.find();

        res.status(200).json({ allOrders });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error', error: error.message });
    }
};

//-----------------------------------------------------------------------s54 end