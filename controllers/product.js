const Product = require('../models/product');

// s51   Create Product (Admin only)
// module.exports.createProduct = async (req, res) => {
//     try {
//         const { name, description, price } = req.body;

//         const newProduct = new Product({
//             name: req.body.name,
//             description: req.body.description,
//             price: req.body.price,
//             isActive: true // By default, a newly created product is active
//         });

//         const savedProduct = await newProduct.save();

//         res.status(201).json({ message: 'Product created successfully', product: savedProduct });
//     } catch (error) {
//         console.error(error);
//         res.status(500).json({ message: 'Internal server error' });
//     }
// };

module.exports.createProduct = (req, res) => {
    Product.findOne({ name: req.body.name })
        .then(existingProduct => {
            if (existingProduct) {
                return res.status(409).send({ error: "The Product is already existing" });
            } else {
                let newProduct = new Product({
                    name: req.body.name,
                    description: req.body.description,
                    price: req.body.price
                });

                return newProduct.save()
                    .then(savedProduct => res.status(201).send({ savedProduct }))
                    .catch(err => {
                        console.error("Error in adding the product", err);
                        return res.status(500).send({ error: "Failed to add the product" });
                    });
            }
        })
        .catch(err => {
            console.error("Error in finding the product", err);
            return res.status(500).send({ error: "Internal Server Error" });
        });
};

// s51  Retrieve all products (Admin only)
module.exports.getAllProducts = async (req, res) => {
    try {
        const products = await Product.find();

        res.status(200).json({ products });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
};

// s51  Retrieve all active products (All users)
module.exports.getActiveProducts = async (req, res) => {
    try {
        const activeProducts = await Product.find({ isActive: true });

        res.status(200).json({ activeProducts });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
};

// s51  Retrieve a single product (All users)
module.exports.getSingleProduct = async (req, res) => {
    try {
        const productId = req.params.productId;
        const product = await Product.findById(productId);

        if (!product) {
            return res.status(404).json({ message: 'Product not found' });
        }

        res.status(200).json({ product });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
};

// s51  Update product info (Admin only)
module.exports.updateProduct = async (req, res) => {
    try {
        const productId = req.params.productId;
        const updatedProduct = await Product.findByIdAndUpdate(
            productId,
            { $set: req.body },
            { new: true }
        );

        if (!updatedProduct) {
            return res.status(404).json({ message: 'Product not found' });
        }

        res.status(200).json({ message: 'Product updated successfully', product: updatedProduct });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
};

// s51  Archive product (Admin only)
module.exports.archiveProduct = async (req, res) => {
    try {
        const productId = req.params.productId;
        const archivedProduct = await Product.findByIdAndUpdate(
            productId,
            { isActive: false },
            { new: true }
        );

        if (!archivedProduct) {
            return res.status(404).json({ message: 'Product not found' });
        }

        res.status(200).json({ message: 'Product archived successfully', product: archivedProduct });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
};

// s51  Activate product (Admin only)
module.exports.activateProduct = async (req, res) => {
    try {
        const productId = req.params.productId;
        const activatedProduct = await Product.findByIdAndUpdate(
            productId,
            { isActive: true },
            { new: true }
        );

        if (!activatedProduct) {
            return res.status(404).json({ message: 'Product not found' });
        }

        res.status(200).json({ message: 'Product activated successfully', product: activatedProduct });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
};

//-------------------------------------------------------------------------------------- s53 start


// Search for products by name
module.exports.searchProductByName = (req, res) => {
    const { productName } = req.body;

    // Search for products by name
    Product.find({ name: { $regex: productName, $options: 'i' } })
        .then((products) => {
            res.status(200).json({ products });
        })
        .catch((err) => {
            console.error(err);
            res.status(500).json({ message: "Internal server error", error: err });
        });
};



module.exports.searchProductByPrice = (req, res) => {
    const { minPrice, maxPrice } = req.body;

    console.log('minPrice:', minPrice);
    console.log('maxPrice:', maxPrice);

    // Check if minPrice and maxPrice are provided
    if (!minPrice || !maxPrice) {
        return res.status(400).json({ message: 'Both minPrice and maxPrice are required' });
    }

    // Convert the parameters to numbers
    const min = parseFloat(minPrice);
    const max = parseFloat(maxPrice);

    // Find products within the price range
    Product.find({ price: { $gte: min, $lte: max } })
        .then((products) => {
            res.status(200).json(products);
        })
        .catch((err) => {
            console.error(err);
            res.status(500).json({ message: 'Internal server error' });
        });
};

//-------------------------------------------------------------------------------------- s53 end

